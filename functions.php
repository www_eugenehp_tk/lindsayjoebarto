<?php
/**
 * Define and require all the necessary 'bits and pieces'
 * and build all necessary Static Homepage and Featured area functions.
 *
 * @package rt
 */

/**
 * Call Genesis's core functions.
 */
require_once( get_template_directory() . '/lib/init.php' );

/**
 * Define child theme constants.
 */
define( 'CHILD_THEME_NAME', 'Eyeworks' );
define( 'CHILD_THEME_URL', 'http://round2.us' );
define( 'CHILD_THEME_VERSION', '1.0' );

add_filter( 'avatar_defaults', 'child_default_avatar' );
/**
 * Display a Custom Avatar if one exists with the correct name
 * and in the correct images directory.
 */
function child_default_avatar( $avatar_defaults )
{
	$custom_avatar_image = '';
	if( file_exists( CHILD_DIR . '/images/custom-avatar.png' ) )
		$custom_avatar_image = CHILD_URL . '/images/custom-avatar.png';
	elseif( file_exists( CHILD_DIR . '/images/custom-avatar.jpg' ) )
		$custom_avatar_image = CHILD_URL . '/images/custom-avatar.jpg';
	elseif( file_exists( CHILD_DIR . '/images/custom-avatar.gif' ) )
		$custom_avatar_image = CHILD_URL . '/images/custom-avatar.gif';
	elseif( file_exists( CHILD_DIR . '/images/custom-avatar.jpg' ) )
		$custom_avatar_image = CHILD_URL . '/images/custom-avatar.jpg';

	$custom_avatar = apply_filters( 'child_custom_avatar_path', $custom_avatar_image );
	$avatar_defaults[$custom_avatar] = 'Custom Avatar';
	
	return $avatar_defaults;
}

/**
 * Manage the placement of navbars.
 */
add_action( 'genesis_after_header', 'child_dropdown_nav_1' );
add_action( 'genesis_after_header', 'child_dropdown_nav_2' );

/**
 * Register the additional Responsive Dropdown Menus.
 */
add_theme_support( 'genesis-menus', array( 'primary' => __( 'Primary Navigation Menu', 'rt' ), 'secondary' => __( 'Secondary Navigation Menu', 'rt' ), 'primary_dropdown' => __( 'Responsive Dropdown 1', 'rt' ), 'secondary_dropdown' => __( 'Responsive Dropdown 2', 'rt' ) ) );

/**
 * Build Nav Dropdown HTML.
 */
function child_dropdown_nav_1() {
	if ( ! has_nav_menu( 'primary_dropdown' ) )
		return;
?>
	<div id="dropdown-nav-wrap">
		<!-- dropdown nav for responsive design -->
		<nav id="dropdown-nav" role="navigation">
			<?php dynamik_dropdown_menu_1( array( 'theme_location' => 'primary_dropdown', 'dropdown_title' => 'Nav' ) ); ?>
			<div class="responsive-menu-icon">
				<span class="responsive-icon-bar">
</span>
				<span class="responsive-icon-bar">
</span>
				<span class="responsive-icon-bar">
</span>
			</div>
		</nav>
<!-- #dropdown-nav -->
		<!-- /end dropdown nav -->
	</div>
<?php
}

/**
 * Build Subnav Dropdown HTML.
 */
function child_dropdown_nav_2() {
	if ( ! has_nav_menu( 'secondary_dropdown' ) )
		return;
?>
	<div id="dropdown-subnav-wrap">	
		<!-- dropdown nav for responsive design -->
		<nav id="dropdown-subnav" role="navigation">
			<?php dynamik_dropdown_menu_2( array( 'theme_location' => 'secondary_dropdown', 'dropdown_title' => 'Navigation' ) ); ?>
			<div class="responsive-menu-icon">
				<span class="responsive-icon-bar">
</span>
				<span class="responsive-icon-bar">
</span>
				<span class="responsive-icon-bar">
</span>
			</div>
		</nav>
<!-- #dropdown-subnav -->
		<!-- /end dropdown subnav -->
	</div>
<?php
}

/**
 * The following edited dropdown menu code was
 * pulled from the following WordPress Plugin:
 * http://wordpress.org/plugins/dropdown-menus/
 */

/**
 * Tack on the blank option for urls not in the menu
 */
add_filter( 'wp_nav_menu_items', 'dropdown_add_blank_item', 10, 2 );
function dropdown_add_blank_item( $items, $args ) {
	if ( isset( $args->walker ) && is_object( $args->walker ) && method_exists( $args->walker, 'is_dropdown' ) ) {
		if ( ( ! isset( $args->menu ) || empty( $args->menu ) ) && isset( $args->theme_location ) ) {
			$theme_locations = get_nav_menu_locations();
			$args->menu = wp_get_nav_menu_object( $theme_locations[ $args->theme_location ] );
		}
		$title = isset( $args->dropdown_title ) ? wptexturize( $args->dropdown_title ) : '&mdash; ' . $args->menu->name . ' &mdash;';
		if ( !empty( $title ) )
			$items = '<option value="" class="blank">' . apply_filters( 'dropdown_blank_item_text', $title, $args ) . '</option>' . $items;
	}
	return $items;
}

/**
 * Remove empty options created in the sub levels output
 */
add_filter( 'wp_nav_menu_items', 'dropdown_remove_empty_items', 10, 2 );
function dropdown_remove_empty_items( $items, $args ) {
	if ( isset( $args->walker ) && is_object( $args->walker ) && method_exists( $args->walker, 'is_dropdown' ) )
		$items = str_replace( "<option>
</option>", "", $items );
	return $items;
}

/**
 * Overrides the walker argument and container argument then calls wp_nav_menu
 */
function dynamik_dropdown_menu_1( $args ) {
	// if non array supplied use as theme location
	if ( ! is_array( $args ) )
		$args = array( 'menu' => $args );

	// enforce these arguments so it actually works
	$args[ 'walker' ] = new Dynamik_DropDown_Nav_Menu();
	$args[ 'items_wrap' ] = '<select id="%1$s" class="%2$s mobile-dropdown-menu nav-chosen-select">%3$s</select>';

	// custom args for controlling indentation of sub menu items
	$args[ 'indent_string' ] = isset( $args[ 'indent_string' ] ) ? $args[ 'indent_string' ] : '&ndash;&nbsp;';
	$args[ 'indent_after' ] =  isset( $args[ 'indent_after' ] ) ? $args[ 'indent_after' ] : '';

	return wp_nav_menu( $args );
}

/**
 * Overrides the walker argument and container argument then calls wp_nav_menu
 */
function dynamik_dropdown_menu_2( $args ) {
	// if non array supplied use as theme location
	if ( ! is_array( $args ) )
		$args = array( 'menu' => $args );

	// enforce these arguments so it actually works
	$args[ 'walker' ] = new Dynamik_DropDown_Nav_Menu();
	$args[ 'items_wrap' ] = '<select id="%1$s" class="%2$s mobile-dropdown-menu subnav-chosen-select">%3$s</select>';

	// custom args for controlling indentation of sub menu items
	$args[ 'indent_string' ] = isset( $args[ 'indent_string' ] ) ? $args[ 'indent_string' ] : '&ndash;&nbsp;';
	$args[ 'indent_after' ] =  isset( $args[ 'indent_after' ] ) ? $args[ 'indent_after' ] : '';

	return wp_nav_menu( $args );
}

class Dynamik_DropDown_Nav_Menu extends Walker_Nav_Menu {

	// easy way to check it's this walker we're using to mod the output
	function is_dropdown() {
		return true;
	}

	/**
	 * @see Walker::start_lvl()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int $depth Depth of page. Used for padding.
	 */
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= "</option>";
	}

	/**
	 * @see Walker::end_lvl()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int $depth Depth of page. Used for padding.
	 */
	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= "<option>";
	}

	/**
	 * @see Walker::start_el()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Menu item data object.
	 * @param int $depth Depth of menu item. Used for padding.
	 * @param int $current_page Menu item ID.
	 * @param object $args
	 */
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;
		$classes[] = 'menu-item-depth-' . $depth;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_unique( array_filter( $classes ) ), $item, $args ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';

		$selected = '';

		// select current item
		if ( apply_filters( 'dropdown_menus_select_current', true ) )
			$selected = in_array( 'current-menu-item', $classes ) ? ' selected="selected"' : '';

		$output .= $indent . '<option' . $class_names .' value="'. $item->url .'"'. $selected .'>';

		// push sub-menu items in as we can't nest optgroups
		$indent_string = str_repeat( apply_filters( 'dropdown_menus_indent_string', $args->indent_string, $item, $depth, $args ), ( $depth ) ? $depth : 0 );
		$indent_string .= !empty( $indent_string ) ? apply_filters( 'dropdown_menus_indent_after', $args->indent_after, $item, $depth, $args ) : '';

		$item_output = $args->before . $indent_string;
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_dropdown_start_el', $item_output, $item, $depth, $args );
	}

	/**
	 * @see Walker::end_el()
	 * @since 3.0.0
	 */
	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= apply_filters( 'walker_nav_menu_dropdown_end_el', "</option>\n", $item, $depth);
	}
}

add_filter( 'dropdown_menus_select_current', create_function( '$bool', 'return false;' ) );

/**
 * END WordPress dropdown Plugin code.
 */

add_action( 'genesis_meta', 'child_responsive_viewport' );
/**
 * Add viewport meta tag to the genesis_meta hook
 * to force 'real' scale of site when viewed in mobile devices.
 *
 * @since 1.0
 */
function child_responsive_viewport() {
echo '<meta name="viewport" content="width=device-width, initial-scale=1.0"/>' . "\n";
}
			
add_action( 'get_header', 'child_remove_page_titles' );
/**
 * Remove all page titles.
 *
 * @since 1.0
 */
function child_remove_page_titles() {
    if ( is_page() && ! is_page_template( 'page_blog.php' ) )
        remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
}

/**
 * Add support for Genesis HTML5 Markup.
 */
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

/**
 * Add support for Genesis 'Fancy Dropdowns'.
 */
add_filter( 'genesis_superfish_enabled', '__return_true' );

add_filter( 'genesis_author_box_gravatar_size', 'child_author_box_gravatar_size' );
/**
 * Modify the size of the Gravatar in the author box.
 *
 * @since 1.0
 */
function child_author_box_gravatar_size( $size )
{
	return 160;
}

add_filter( 'genesis_comment_list_args', 'child_comments_gravatar_size' );
/**
 * Modify the size of the Gravatar in comments.
 *
 * @since 1.0
 */
function child_comments_gravatar_size( $args )
{
	$args['avatar_size'] = 96;
	return $args;
}

/**
 * This is altered version of the genesis_get_custom_field() function
 * which includes the additional ability to work with array() values.
 *
 * @since 1.0
 */
function dynamik_get_custom_field( $field, $single = true, $explode = false )
{
	if( null === get_the_ID() )
		return '';

	$custom_field = get_post_meta( get_the_ID(), $field, $single );

	if( !$custom_field )
		return '';

	if( !$single )
	{
		$custom_field_string = implode( ',', $custom_field );
		if( $explode )
		{
			$custom_field_array_pre = explode( ',', $custom_field_string );
			foreach( $custom_field_array_pre as $key => $value )
			{
				$custom_field_array[$value] = $value;
			}
			return $custom_field_array;
		}
		return $custom_field_string;
	}

	return is_array( $custom_field ) ? stripslashes_deep( $custom_field ) : stripslashes( wp_kses_decode_entities( $custom_field ) );
}

/**
 * Create a dynamik Label conditional tag which
 * allows content to be conditionally placed on pages and posts
 * that have specific dynamik Labels assigned to them.
 *
 * @since 1.0
 */
function dynamik_has_label( $label = 'label' )
{
	$labels_meta_array = dynamik_get_custom_field( '_dyn_labels', false, true ) != '' ? dynamik_get_custom_field( '_dyn_labels', false, true ) : array();

	if( is_singular() )
	{
		if( in_array( $label, $labels_meta_array ) ) return true;
	}

	return false;
}

/**
 * Create a Genesis Simple Sidebars conditional tag which
 * allows content to be conditionally placed on pages and posts
 * that have specific simple sidebars assigned to them.
 *
 * @since 1.0
 */
function dynamik_is_ss( $sidebar_id = 'sb-id' )
{
	if( !defined( 'SS_SETTINGS_FIELD' ) )
		return false;

	static $taxonomies = null;

	if( is_singular() )
	{
		if( $sidebar_id == genesis_get_custom_field( '_ss_sidebar' ) ) return true;
	}

	if( is_category() )
	{
		$term = get_term( get_query_var( 'cat' ), 'category' );
		if( isset( $term->meta['_ss_sidebar'] ) && $sidebar_id == $term->meta['_ss_sidebar'] ) return true;
	}

	if( is_tag() )
	{
		$term = get_term( get_query_var( 'tag_id' ), 'post_tag' );
		if( isset( $term->meta['_ss_sidebar'] ) && $sidebar_id == $term->meta['_ss_sidebar'] ) return true;
	}

	if( is_tax() )
	{
		if ( null === $taxonomies )
			$taxonomies = ss_get_taxonomies();

		foreach ( $taxonomies as $tax )
		{
			if ( 'post_tag' == $tax || 'category' == $tax )
				continue;

			if ( is_tax( $tax ) )
			{
				$obj = get_queried_object();
				$term = get_term( $obj->term_id, $tax );
				if( isset( $term->meta['_ss_sidebar'] ) && $sidebar_id == $term->meta['_ss_sidebar'] ) return true;
				break;
			}
		}
	}

	return false;
}

/**
 * Enable Shortcodes in Text Widgets.
 */
add_filter( 'widget_text', 'do_shortcode' );

if( is_admin() )
{
	add_filter( 'cmb_meta_boxes', 'child_lables_metabox' );
	/**
	 * Define the metabox and field configurations.
	 *
	 * @since 1.0
	 * @return array
	 */
	function child_lables_metabox( array $meta_boxes )
	{
		// Start with an underscore to hide fields from custom fields list
		$prefix = '_dyn_';
		$post_type_array = explode( ',', 'page,post' );

		$meta_boxes[] = array(
			'id'         => 'dynamik_labels',
			'title'      => 'rt Labels',
			'pages'      => $post_type_array, // Post type
			'context'    => 'normal',
			'priority'   => 'high',
			'show_names' => true, // Show field names on the left
			'fields'     => array(
				array(
					'name'    => 'Select Labels',
					'desc'    => 'Select labels appropriate to this page/post.',
					'id'      => $prefix . 'labels',
					'type'    => 'multicheck',
					'options' => array(
						'eye-care-page' => 'eye-care-page','faq' => 'faq','home-page' => 'home-page','reviews' => 'reviews',
					),
				),
			),
		);

		return $meta_boxes;
	}

	add_action( 'init', 'child_initialize_cmb_meta_boxes', 9999 );
	/**
	 * Initialize the metabox class.
	 * @since 1.0
	 */
	function child_initialize_cmb_meta_boxes()
	{
		if( !class_exists( 'cmb_Meta_Box' ) )
			require_once CHILD_DIR . '/metaboxes/init.php';
	}
}

/**
 * Hook the Fat Footer Structure function into the 'wp_head' Hook.
 */
add_action( 'wp_head', 'child_fat_footer' );

/**
 * Determine where NOT to display the Fat Footer section before hooking it in.
 *
 * @since 1.0
 */
function child_fat_footer() {
	/**
	 * Add conditional tags to control where the Fat Footer Widget Area displays.
	 */
	if ( is_page_template( 'landing.php' ) ) { return; }
	    
	
	/**
	 * Hook the Fat Footer Structure function into the appropriate Genesis Hook.
	 */
	add_action( 'genesis_after_content_sidebar_wrap', 'ez_fat_footer' );
}
/*
 * Register EZ Widget Areas
 */
genesis_register_sidebar( array (
	'name'	=>	'EZ Fat Footer 1',
	'id' 	=> 	'dynamik_ez_fat_footer_1'
) );
		
genesis_register_sidebar( array (
	'name'	=>	'EZ Fat Footer 2',
	'id' 	=> 	'dynamik_ez_fat_footer_2'
) );
		
genesis_register_sidebar( array (
	'name'	=>	'EZ Fat Footer 3',
	'id' 	=> 	'dynamik_ez_fat_footer_3'
) );
		
/**
 * Build the EZ Fat Footer HTML.
 *
 * @since 1.0
 */
function ez_fat_footer() { ?>
	<div id="ez-fat-footer-container-wrap" class="clearfix">
	
		<div id="ez-fat-footer-container" class="clearfix">
	
			<div id="ez-fat-footer-1" class="widget-area ez-widget-area one-third first">
				<?php if ( ! dynamic_sidebar( 'EZ Fat Footer #1' ) ) { ?>
					<div class="widget">
						<h4>
<?php _e( 'EZ Fat Footer #1', 'rt' ); ?>
</h4>
						<p>
<?php printf( __( 'This is rt Widget Area. You can add content to this area by going to <a href="%s">Appearance > Widgets</a> in your WordPress Dashboard and adding new widgets to this area.', "rt" ), admin_url( "widgets.php" ) ); ?>
</p>
					</div>			
				<?php } ?>
			</div>
<!-- end #fat-footer-1 -->
	
			<div id="ez-fat-footer-2" class="widget-area ez-widget-area one-third">
				<?php if ( ! dynamic_sidebar( 'EZ Fat Footer #2' ) ) { ?>
					<div class="widget">
						<h4>
<?php _e( 'EZ Fat Footer #2', 'rt' ); ?>
</h4>
						<p>
<?php printf( __( 'This is rt Widget Area. You can add content to this area by going to <a href="%s">Appearance > Widgets</a> in your WordPress Dashboard and adding new widgets to this area.', "rt" ), admin_url( "widgets.php" ) ); ?>
</p>
					</div>			
				<?php } ?>
			</div>
<!-- end #fat-footer-2 -->
	
			<div id="ez-fat-footer-3" class="widget-area ez-widget-area one-third">
				<?php if ( ! dynamic_sidebar( 'EZ Fat Footer #3' ) ) { ?>
					<div class="widget">
						<h4>
<?php _e( 'EZ Fat Footer #3', 'rt' ); ?>
</h4>
						<p>
<?php printf( __( 'This is rt Widget Area. You can add content to this area by going to <a href="%s">Appearance > Widgets</a> in your WordPress Dashboard and adding new widgets to this area.', "rt" ), admin_url( "widgets.php" ) ); ?>
</p>
					</div>			
				<?php } ?>
			</div>
<!-- end #fat-footer-3 -->
					
		</div>
<!-- end #fat-footer-container -->
		
	</div>
<!-- end #fat-footer-container-wrap -->
<?php }

/**
 * Register Custom Widget Areas.
 */

genesis_register_sidebar( array(
	'id' 			=>	'blog_left',
	'name'			=>	__( 'Blog Left', 'rt' ),
	'description' 	=>	__( 'This widget will appear below blog page and sit to the left side of the page. ', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'blog_middle',
	'name'			=>	__( 'Blog Middle', 'rt' ),
	'description' 	=>	__( 'This widget will appear below blog page and sit to the middle of the page. ', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'blog_page_title',
	'name'			=>	__( 'Blog Page Title', 'rt' ),
	'description' 	=>	__( 'The content in this widget will appear at the top of the blog page. ', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'blog_right',
	'name'			=>	__( 'Blog Right', 'rt' ),
	'description' 	=>	__( 'This widget will appear below blog page and sit to the right side of the page. ', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'eye_care_left',
	'name'			=>	__( 'Eye Care Left', 'rt' ),
	'description' 	=>	__( 'This widget will appear below Insurance &amp; Financing on the Eye Care Page and sit on the left side of the page. ', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'eye_care_middle',
	'name'			=>	__( 'Eye Care Middle', 'rt' ),
	'description' 	=>	__( 'This widget will appear below Insurance &amp; Financing on the Eye Care Page and sit in the middle of the page. ', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'eye_care_right',
	'name'			=>	__( 'Eye Care Right', 'rt' ),
	'description' 	=>	__( 'This widget will appear below Insurance &amp; Financing on the Eye Care Page and sit in the right side of the page. ', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'eye_care_slider',
	'name'			=>	__( 'Eye Care Slider', 'rt' ),
	'description' 	=>	__( 'Select slider for the Eye Care Page', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'eye_wear_hero',
	'name'			=>	__( 'Eye Wear Hero', 'rt' ),
	'description' 	=>	__( '', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'eye_wear_left',
	'name'			=>	__( 'Eye Wear Left', 'rt' ),
	'description' 	=>	__( '', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'eye_wear_middle',
	'name'			=>	__( 'Eye Wear Middle', 'rt' ),
	'description' 	=>	__( '', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'eye_wear_right',
	'name'			=>	__( 'Eye Wear Right', 'rt' ),
	'description' 	=>	__( '', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'faq_left',
	'name'			=>	__( 'Faq Left', 'rt' ),
	'description' 	=>	__( 'This widget will appear below faq page and sit to the left side of the page. ', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'faq_middle',
	'name'			=>	__( 'Faq Middle', 'rt' ),
	'description' 	=>	__( 'This widget will appear below faq page and sit in the middle of the page. ', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'faq_right',
	'name'			=>	__( 'Faq Right', 'rt' ),
	'description' 	=>	__( 'This widget will appear below faq page and sit to the right side of the page. ', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'home_body_1',
	'name'			=>	__( 'Home Body 1', 'rt' ),
	'description' 	=>	__( 'First widget area below the hero section.', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'home_bottom_1',
	'name'			=>	__( 'Home Bottom 1', 'rt' ),
	'description' 	=>	__( '', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'home_bottom_2',
	'name'			=>	__( 'Home Bottom 2', 'rt' ),
	'description' 	=>	__( '', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'home_bottom_3',
	'name'			=>	__( 'Home Bottom 3', 'rt' ),
	'description' 	=>	__( '', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'home_hero',
	'name'			=>	__( 'Home Hero', 'rt' ),
	'description' 	=>	__( 'This is the large hero area on the homepage template. ', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'home_map',
	'name'			=>	__( 'Home Map', 'rt' ),
	'description' 	=>	__( 'Add the Map shortcode to display a map on the homepage. ', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'home_middle_left',
	'name'			=>	__( 'Home Middle Left', 'rt' ),
	'description' 	=>	__( '', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'home_middle_right',
	'name'			=>	__( 'Home Middle Right', 'rt' ),
	'description' 	=>	__( '', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'our_commitment_hero',
	'name'			=>	__( 'Our Commitment Hero', 'rt' ),
	'description' 	=>	__( '', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'our_commitment_left',
	'name'			=>	__( 'Our Commitment Left', 'rt' ),
	'description' 	=>	__( '', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'our_commitment_middle',
	'name'			=>	__( 'Our Commitment Middle', 'rt' ),
	'description' 	=>	__( '', 'rt' )
) );

genesis_register_sidebar( array(
	'id' 			=>	'our_commitment_right',
	'name'			=>	__( 'Our Commitment Right', 'rt' ),
	'description' 	=>	__( '', 'rt' )
) );

/**
 * Build and Hook-In Custom Widget Areas.
 */

/* Name: Home Bottom 2 */

add_shortcode( 'home_bottom_2', 'dynamik_home_bottom_2_widget_area_shortcode' );
function dynamik_home_bottom_2_widget_area_shortcode() {
	ob_start();
	dynamik_home_bottom_2_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_home_bottom_2_widget_area_content() {
	genesis_widget_area( 'home_bottom_2', $args = array (
		'before'              => '<div id="home_bottom_2" class="widget-area rt-widget-area one-third widget-border">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_home_bottom_2_widget_area',
		'after_sidebar_hook'  => 'genesis_after_home_bottom_2_widget_area'
	) );
}

/* Name: Home Bottom 1 */

add_shortcode( 'home_bottom_1', 'dynamik_home_bottom_1_widget_area_shortcode' );
function dynamik_home_bottom_1_widget_area_shortcode() {
	ob_start();
	dynamik_home_bottom_1_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_home_bottom_1_widget_area_content() {
	genesis_widget_area( 'home_bottom_1', $args = array (
		'before'              => '<div id="home_bottom_1" class="widget-area rt-widget-area one-third first widget-border">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_home_bottom_1_widget_area',
		'after_sidebar_hook'  => 'genesis_after_home_bottom_1_widget_area'
	) );
}

/* Name: Home Middle Right */

add_shortcode( 'home_middle_right', 'dynamik_home_middle_right_widget_area_shortcode' );
function dynamik_home_middle_right_widget_area_shortcode() {
	ob_start();
	dynamik_home_middle_right_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_home_middle_right_widget_area_content() {
	genesis_widget_area( 'home_middle_right', $args = array (
		'before'              => '<div id="home_middle_right" class="widget-area rt-widget-area one-half shop-info ">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_home_middle_right_widget_area',
		'after_sidebar_hook'  => 'genesis_after_home_middle_right_widget_area'
	) );
}

/* Name: Home Middle Left */

add_shortcode( 'home_middle_left', 'dynamik_home_middle_left_widget_area_shortcode' );
function dynamik_home_middle_left_widget_area_shortcode() {
	ob_start();
	dynamik_home_middle_left_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_home_middle_left_widget_area_content() {
	genesis_widget_area( 'home_middle_left', $args = array (
		'before'              => '<div id="home_middle_left" class="widget-area rt-widget-area one-half first ">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_home_middle_left_widget_area',
		'after_sidebar_hook'  => 'genesis_after_home_middle_left_widget_area'
	) );
}

/* Name: Home Body 1 */

add_shortcode( 'home_body_1', 'dynamik_home_body_1_widget_area_shortcode' );
function dynamik_home_body_1_widget_area_shortcode() {
	ob_start();
	dynamik_home_body_1_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_home_body_1_widget_area_content() {
	genesis_widget_area( 'home_body_1', $args = array (
		'before'              => '<div id="home_body_1" class="widget-area rt-widget-area home-intro ">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_home_body_1_widget_area',
		'after_sidebar_hook'  => 'genesis_after_home_body_1_widget_area'
	) );
}

/* Name: Home Hero */

add_action( 'genesis_after_header', 'dynamik_home_hero_widget_area', 10 );
function dynamik_home_hero_widget_area() {
	dynamik_home_hero_widget_area_content();
}

function dynamik_home_hero_widget_area_content() {
	if ( is_page_template('my-templates/home-page.php') ) {
		genesis_widget_area( 'home_hero', $args = array (
			'before'              => '<div id="home_hero" class="widget-area rt-widget-area home-slider">',
			'after'               => '</div>',
			'before_sidebar_hook' => 'genesis_before_home_hero_widget_area',
			'after_sidebar_hook'  => 'genesis_after_home_hero_widget_area'
		) );
	} else {
		return false;
	}
}

/* Name: Home Bottom 3 */

add_shortcode( 'home_bottom_3', 'dynamik_home_bottom_3_widget_area_shortcode' );
function dynamik_home_bottom_3_widget_area_shortcode() {
	ob_start();
	dynamik_home_bottom_3_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_home_bottom_3_widget_area_content() {
	genesis_widget_area( 'home_bottom_3', $args = array (
		'before'              => '<div id="home_bottom_3" class="widget-area rt-widget-area one-third widget-border">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_home_bottom_3_widget_area',
		'after_sidebar_hook'  => 'genesis_after_home_bottom_3_widget_area'
	) );
}

/* Name: Home Map */

add_shortcode( 'home_map', 'dynamik_home_map_widget_area_shortcode' );
function dynamik_home_map_widget_area_shortcode() {
	ob_start();
	dynamik_home_map_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_home_map_widget_area_content() {
	genesis_widget_area( 'home_map', $args = array (
		'before'              => '<div id="home_map" class="widget-area rt-widget-area home-map">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_home_map_widget_area',
		'after_sidebar_hook'  => 'genesis_after_home_map_widget_area'
	) );
}

/* Name: Eye Care Slider */

add_action( 'genesis_after_header', 'dynamik_eye_care_slider_widget_area', 10 );
function dynamik_eye_care_slider_widget_area() {
	dynamik_eye_care_slider_widget_area_content();
}

function dynamik_eye_care_slider_widget_area_content() {
	if ( dynamik_has_label('eye-care-page') ) {
		genesis_widget_area( 'eye_care_slider', $args = array (
			'before'              => '<div id="eye_care_slider" class="widget-area rt-widget-area eye-care">',
			'after'               => '</div>',
			'before_sidebar_hook' => 'genesis_before_eye_care_slider_widget_area',
			'after_sidebar_hook'  => 'genesis_after_eye_care_slider_widget_area'
		) );
	} else {
		return false;
	}
}

/* Name: Eye Care Left */

add_shortcode( 'eye_care_left', 'dynamik_eye_care_left_widget_area_shortcode' );
function dynamik_eye_care_left_widget_area_shortcode() {
	ob_start();
	dynamik_eye_care_left_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_eye_care_left_widget_area_content() {
	genesis_widget_area( 'eye_care_left', $args = array (
		'before'              => '<div id="eye_care_left" class="widget-area rt-widget-area one-third first widget-border">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_eye_care_left_widget_area',
		'after_sidebar_hook'  => 'genesis_after_eye_care_left_widget_area'
	) );
}

/* Name: Eye Care Middle */

add_shortcode( 'eye_care_middle', 'dynamik_eye_care_middle_widget_area_shortcode' );
function dynamik_eye_care_middle_widget_area_shortcode() {
	ob_start();
	dynamik_eye_care_middle_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_eye_care_middle_widget_area_content() {
	genesis_widget_area( 'eye_care_middle', $args = array (
		'before'              => '<div id="eye_care_middle" class="widget-area rt-widget-area one-third widget-border">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_eye_care_middle_widget_area',
		'after_sidebar_hook'  => 'genesis_after_eye_care_middle_widget_area'
	) );
}

/* Name: Eye Care Right */

add_shortcode( 'eye_care_right', 'dynamik_eye_care_right_widget_area_shortcode' );
function dynamik_eye_care_right_widget_area_shortcode() {
	ob_start();
	dynamik_eye_care_right_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_eye_care_right_widget_area_content() {
	genesis_widget_area( 'eye_care_right', $args = array (
		'before'              => '<div id="eye_care_right" class="widget-area rt-widget-area one-third widget-border">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_eye_care_right_widget_area',
		'after_sidebar_hook'  => 'genesis_after_eye_care_right_widget_area'
	) );
}

/* Name: Eye Wear Left */

add_shortcode( 'eye_wear_left', 'dynamik_eye_wear_left_widget_area_shortcode' );
function dynamik_eye_wear_left_widget_area_shortcode() {
	ob_start();
	dynamik_eye_wear_left_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_eye_wear_left_widget_area_content() {
	genesis_widget_area( 'eye_wear_left', $args = array (
		'before'              => '<div id="eye_wear_left" class="widget-area rt-widget-area one-third first widget-border">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_eye_wear_left_widget_area',
		'after_sidebar_hook'  => 'genesis_after_eye_wear_left_widget_area'
	) );
}

/* Name: Eye Wear Middle */

add_shortcode( 'eye_wear_middle', 'dynamik_eye_wear_middle_widget_area_shortcode' );
function dynamik_eye_wear_middle_widget_area_shortcode() {
	ob_start();
	dynamik_eye_wear_middle_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_eye_wear_middle_widget_area_content() {
	genesis_widget_area( 'eye_wear_middle', $args = array (
		'before'              => '<div id="eye_wear_middle" class="widget-area rt-widget-area one-third widget-border">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_eye_wear_middle_widget_area',
		'after_sidebar_hook'  => 'genesis_after_eye_wear_middle_widget_area'
	) );
}

/* Name: Eye Wear Right */

add_shortcode( 'eye_wear_right', 'dynamik_eye_wear_right_widget_area_shortcode' );
function dynamik_eye_wear_right_widget_area_shortcode() {
	ob_start();
	dynamik_eye_wear_right_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_eye_wear_right_widget_area_content() {
	genesis_widget_area( 'eye_wear_right', $args = array (
		'before'              => '<div id="eye_wear_right" class="widget-area rt-widget-area one-third widget-border">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_eye_wear_right_widget_area',
		'after_sidebar_hook'  => 'genesis_after_eye_wear_right_widget_area'
	) );
}

/* Name: Eye Wear Hero */

add_action( 'genesis_after_header', 'dynamik_eye_wear_hero_widget_area', 10 );
function dynamik_eye_wear_hero_widget_area() {
	dynamik_eye_wear_hero_widget_area_content();
}

function dynamik_eye_wear_hero_widget_area_content() {
	if ( is_page_template('my-templates/eye-wear.php') ) {
		genesis_widget_area( 'eye_wear_hero', $args = array (
			'before'              => '<div id="eye_wear_hero" class="widget-area rt-widget-area">',
			'after'               => '</div>',
			'before_sidebar_hook' => 'genesis_before_eye_wear_hero_widget_area',
			'after_sidebar_hook'  => 'genesis_after_eye_wear_hero_widget_area'
		) );
	} else {
		return false;
	}
}

/* Name: Our Commitment Hero */

add_action( 'genesis_after_header', 'dynamik_our_commitment_hero_widget_area', 10 );
function dynamik_our_commitment_hero_widget_area() {
	dynamik_our_commitment_hero_widget_area_content();
}

function dynamik_our_commitment_hero_widget_area_content() {
	if ( is_page_template('my-templates/our-commitment.php') ) {
		genesis_widget_area( 'our_commitment_hero', $args = array (
			'before'              => '<div id="our_commitment_hero" class="widget-area rt-widget-area">',
			'after'               => '</div>',
			'before_sidebar_hook' => 'genesis_before_our_commitment_hero_widget_area',
			'after_sidebar_hook'  => 'genesis_after_our_commitment_hero_widget_area'
		) );
	} else {
		return false;
	}
}

/* Name: Our Commitment Left */

add_shortcode( 'our_commitment_left', 'dynamik_our_commitment_left_widget_area_shortcode' );
function dynamik_our_commitment_left_widget_area_shortcode() {
	ob_start();
	dynamik_our_commitment_left_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_our_commitment_left_widget_area_content() {
	genesis_widget_area( 'our_commitment_left', $args = array (
		'before'              => '<div id="our_commitment_left" class="widget-area rt-widget-area one-third first widget-border">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_our_commitment_left_widget_area',
		'after_sidebar_hook'  => 'genesis_after_our_commitment_left_widget_area'
	) );
}

/* Name: Our Commitment Middle */

add_shortcode( 'our_commitment_middle', 'dynamik_our_commitment_middle_widget_area_shortcode' );
function dynamik_our_commitment_middle_widget_area_shortcode() {
	ob_start();
	dynamik_our_commitment_middle_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_our_commitment_middle_widget_area_content() {
	genesis_widget_area( 'our_commitment_middle', $args = array (
		'before'              => '<div id="our_commitment_middle" class="widget-area rt-widget-area one-third widget-border">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_our_commitment_middle_widget_area',
		'after_sidebar_hook'  => 'genesis_after_our_commitment_middle_widget_area'
	) );
}

/* Name: Our Commitment Right */

add_shortcode( 'our_commitment_right', 'dynamik_our_commitment_right_widget_area_shortcode' );
function dynamik_our_commitment_right_widget_area_shortcode() {
	ob_start();
	dynamik_our_commitment_right_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_our_commitment_right_widget_area_content() {
	genesis_widget_area( 'our_commitment_right', $args = array (
		'before'              => '<div id="our_commitment_right" class="widget-area rt-widget-area one-third widget-border">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_our_commitment_right_widget_area',
		'after_sidebar_hook'  => 'genesis_after_our_commitment_right_widget_area'
	) );
}

/* Name: Blog Page Title */

add_shortcode( 'blog_page_title', 'dynamik_blog_page_title_widget_area_shortcode' );
function dynamik_blog_page_title_widget_area_shortcode() {
	ob_start();
	dynamik_blog_page_title_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_blog_page_title_widget_area_content() {
	genesis_widget_area( 'blog_page_title', $args = array (
		'before'              => '<div id="blog_page_title" class="widget-area rt-widget-area one-half first">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_blog_page_title_widget_area',
		'after_sidebar_hook'  => 'genesis_after_blog_page_title_widget_area'
	) );
}

/* Name: Faq Left */

add_shortcode( 'faq_left', 'dynamik_faq_left_widget_area_shortcode' );
function dynamik_faq_left_widget_area_shortcode() {
	ob_start();
	dynamik_faq_left_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_faq_left_widget_area_content() {
	genesis_widget_area( 'faq_left', $args = array (
		'before'              => '<div id="faq_left" class="widget-area rt-widget-area one-third first widget-border">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_faq_left_widget_area',
		'after_sidebar_hook'  => 'genesis_after_faq_left_widget_area'
	) );
}

/* Name: Faq Middle */

add_shortcode( 'faq_middle', 'dynamik_faq_middle_widget_area_shortcode' );
function dynamik_faq_middle_widget_area_shortcode() {
	ob_start();
	dynamik_faq_middle_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_faq_middle_widget_area_content() {
	genesis_widget_area( 'faq_middle', $args = array (
		'before'              => '<div id="faq_middle" class="widget-area rt-widget-area one-third widget-border">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_faq_middle_widget_area',
		'after_sidebar_hook'  => 'genesis_after_faq_middle_widget_area'
	) );
}

/* Name: Faq Right */

add_shortcode( 'faq_right', 'dynamik_faq_right_widget_area_shortcode' );
function dynamik_faq_right_widget_area_shortcode() {
	ob_start();
	dynamik_faq_right_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_faq_right_widget_area_content() {
	genesis_widget_area( 'faq_right', $args = array (
		'before'              => '<div id="faq_right" class="widget-area rt-widget-area one-third widget-border">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_faq_right_widget_area',
		'after_sidebar_hook'  => 'genesis_after_faq_right_widget_area'
	) );
}

/* Name: Blog Left */

add_shortcode( 'blog_left', 'dynamik_blog_left_widget_area_shortcode' );
function dynamik_blog_left_widget_area_shortcode() {
	ob_start();
	dynamik_blog_left_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_blog_left_widget_area_content() {
	genesis_widget_area( 'blog_left', $args = array (
		'before'              => '<div id="blog_left" class="widget-area rt-widget-area one-third first widget-border">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_blog_left_widget_area',
		'after_sidebar_hook'  => 'genesis_after_blog_left_widget_area'
	) );
}

/* Name: Blog Middle */

add_shortcode( 'blog_middle', 'dynamik_blog_middle_widget_area_shortcode' );
function dynamik_blog_middle_widget_area_shortcode() {
	ob_start();
	dynamik_blog_middle_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_blog_middle_widget_area_content() {
	genesis_widget_area( 'blog_middle', $args = array (
		'before'              => '<div id="blog_middle" class="widget-area rt-widget-area one-third widget-border">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_blog_middle_widget_area',
		'after_sidebar_hook'  => 'genesis_after_blog_middle_widget_area'
	) );
}

/* Name: Blog Right */

add_shortcode( 'blog_right', 'dynamik_blog_right_widget_area_shortcode' );
function dynamik_blog_right_widget_area_shortcode() {
	ob_start();
	dynamik_blog_right_widget_area_content();
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

function dynamik_blog_right_widget_area_content() {
	genesis_widget_area( 'blog_right', $args = array (
		'before'              => '<div id="blog_right" class="widget-area rt-widget-area one-third widget-border">',
		'after'               => '</div>',
		'before_sidebar_hook' => 'genesis_before_blog_right_widget_area',
		'after_sidebar_hook'  => 'genesis_after_blog_right_widget_area'
	) );
}

/**
 * Build and Hook-In Custom Hook Boxes.
 */

/* Name: home_page */

add_action( 'genesis_loop', 'dynamik_home_page_hook_box', 10 );
function dynamik_home_page_hook_box() {
	dynamik_home_page_hook_box_content();
}

function dynamik_home_page_hook_box_content() {
	if ( is_page_template('my-templates/home-page.php') ) { ?>
<div class="middle-row-wrap">
		<?php echo do_shortcode( '[home_body_1]' ); ?>

</div>

<div class="location-row-wrap clearfix">
		<?php echo do_shortcode( '[home_middle_left]' ); ?>
<?php echo do_shortcode( '[home_middle_right]' ); ?>
</div>

<div class="map-row-wrap">
	<?php echo do_shortcode( '[home_map]' ); ?>	
</div>

<div class="bottom-row-wrap clearfix">
	<?php echo do_shortcode( '[home_bottom_1]' ); ?>
<?php echo do_shortcode( '[home_bottom_2]' ); ?>
<?php echo do_shortcode( '[home_bottom_3]' ); ?>	
</div>


	<?php } else {
		return false;
	}
}

/* Name: eye_care */

add_action( 'genesis_loop', 'dynamik_eye_care_hook_box', 10 );
function dynamik_eye_care_hook_box() {
	dynamik_eye_care_hook_box_content();
}

function dynamik_eye_care_hook_box_content() {
	if ( is_page_template('my-templates/eye-care.php') ) { ?>
<div class="top-row-wrap clearfix">
<div class="one-half first">
<?php the_field('full_service'); ?>
</div>

<div class="one-half rt-width">
<?php the_field('services_include'); ?>
</div>
</div>

<div class="eye-middle-row-wrap clearfix">
<?php the_field('insurance_financing'); ?>
</div>

<div class="bottom-row-wrap clearfix">
	<?php echo do_shortcode( '[eye_care_left]' ); ?>
<?php echo do_shortcode( '[eye_care_middle]' ); ?>
<?php echo do_shortcode( '[eye_care_right]' ); ?>	
</div>

	<?php } else {
		return false;
	}
}

/* Name: eye_wear */

add_action( 'genesis_loop', 'dynamik_eye_wear_hook_box', 10 );
function dynamik_eye_wear_hook_box() {
	dynamik_eye_wear_hook_box_content();
}

function dynamik_eye_wear_hook_box_content() {
	if ( is_page_template('my-templates/eye-wear.php') ) { ?>
<div class="eye-wear-top-wrap clearfix">
	<div class="one-third first">
		<?php if( have_rows('product_1')): ?>
			<ul>
				<?php while( have_rows('product_1') ): the_row();
					$title  =  get_sub_field('title');
					$image = get_sub_field('product_image');
					$desc = get_sub_field('product_discription');
					$sku = get_sub_field('sku');
				?>
					<li>
						<?php if( $image ): ?>
							<a href="#<?php echo $sku; ?>" class="fancybox" rel="product">
<?php echo $title; ?>
</a>
						<?php endif; ?>
						<div style="display: none;">
							<div id="<?php echo $sku; ?>">
								<img src="<?php echo $image['url']; ?>" />
								<h1>
<?php echo $title; ?>
</h1>
								<p>
<?php echo $desc; ?>
</p>
							</div>
						</div>
						
					</li>
				<?php endwhile; ?> 
			</ul>
		<?php endif; ?>
	</div>

	<div class="one-third">
		<?php if( have_rows('product_middle_column')): ?>
			<ul>
				<?php while( have_rows('product_middle_column') ): the_row();
					$title  =  get_sub_field('title');
					$image = get_sub_field('product_image');
					$desc = get_sub_field('product_discription');
					$sku = get_sub_field('sku');
				?>
					<li>
						<?php if( $image ): ?>
							<a href="#<?php echo $sku; ?>" class="fancybox" rel="product">
<?php echo $title; ?>
</a>
						<?php endif; ?>
						<div style="display: none">
							<div id="<?php echo $sku; ?>">
								<img src="<?php echo $image['url']; ?>" />
								<h1>
<?php echo $title; ?>
</h1>
								<p>
<?php echo $desc; ?>
</p>
							</div>
						</div>		
					</li>
				<?php endwhile; ?> 
			</ul>
		<?php endif; ?>
	</div>

	<div class="one-third">
		<?php if( have_rows('product_right_column')): ?>
			<ul>
				<?php while( have_rows('product_right_column') ): the_row();
					$title  =  get_sub_field('title');
					$image = get_sub_field('product_image');
					$desc = get_sub_field('product_discription');
					$sku = get_sub_field('sku');
				?>
				<li>
					<?php if( $image ): ?>
						<a href="#<?php echo $sku; ?>" class="fancybox" rel="product">
<?php echo $title; ?>
</a>
					<?php endif; ?>
					<div style="display: none">
						<div id="<?php echo $sku; ?>">
							<img src="<?php echo $image['url']; ?>" />
							<h1>
<?php echo $title; ?>
</h1>
							<p>
<?php echo $desc; ?>
</p>
						</div>
					</div>
					
				</li>
				<?php endwhile; ?> 
			</ul>
		<?php endif; ?>
		<!-- 	Anchor from homepage slider to frames -->
		<div id="frame" name="frame">
</div>
	</div>
</div>
<div  class="rt-slider-wrap">
	<?php echo do_shortcode( '[rev_slider eye-wear]' ); ?>
</div>
<div class="bottom-row-wrap clearfix">
	<?php echo do_shortcode( '[eye_wear_left]' ); ?>
<?php echo do_shortcode( '[eye_wear_middle]' ); ?>
<?php echo do_shortcode( '[eye_wear_right]' ); ?>	
</div>
	<?php } else {
		return false;
	}
}

/* Name: our_commitment */

add_action( 'genesis_loop', 'dynamik_our_commitment_hook_box', 10 );
function dynamik_our_commitment_hook_box() {
	dynamik_our_commitment_hook_box_content();
}

function dynamik_our_commitment_hook_box_content() {
	if ( is_page_template('my-templates/our-commitment.php') ) { ?>
<div class="commitment-top-row-wrap clearfix">
		<?php the_field('commitment'); ?>
</div>

<div class="rt-slider-wrap clearfix">
	<h1>Our Optometrists</h1>
	<?php echo do_shortcode( '[rev_slider our-commitment]' ); ?>
</div>
<div class="bottom-row-wrap clearfix">
	<?php echo do_shortcode( '[our_commitment_left]' ); ?>
<?php echo do_shortcode( '[our_commitment_middle]' ); ?>
<?php echo do_shortcode( '[our_commitment_right]' ); ?>	
</div>

	<?php } else {
		return false;
	}
}

/* Name: faq_page */

add_action( 'genesis_loop', 'dynamik_faq_page_hook_box', 10 );
function dynamik_faq_page_hook_box() {
	dynamik_faq_page_hook_box_content();
}

function dynamik_faq_page_hook_box_content() {
	if ( is_page_template('my-templates/faq.php') ) { ?>
<div class="top-row-wrap clearfix">
	<div class="one-half first">
		<?php the_field('page_description'); ?>
	</div>
</div>
<div class="faq-row-wrap clearfix">
	<div class="one-half first">
		<?php if( have_rows('faq_left_column')): ?>
			<ul>
				<?php while( have_rows('faq_left_column') ): the_row();
					$question_left  =  get_sub_field('question');
					$answer_left = get_sub_field('answer');
					$left_id = get_sub_field('question_id');
				?>
				<li>
					<?php if( $question_left ): ?>
						<a href="#<?php echo $left_id; ?>" class="fancybox" rel="faq">
<?php echo $question_left; ?>
</a>
					<?php endif; ?>
					<div style="display: none">
						<div id="<?php echo $left_id; ?>">
							<h1>
<?php echo $question_left; ?>
</h1>
							<p>
<?php echo $answer_left; ?>
</p>
						</div>
					</div>
					
				</li>
				<?php endwhile; ?> 
			</ul>
		<?php endif; ?>
	</div>
	<div class="one-half">
		<?php if( have_rows('faq_right_column')): ?>
			<ul>
				<?php while( have_rows('faq_right_column') ): the_row();
					$question_right  =  get_sub_field('question');
					$answer_right = get_sub_field('answer');
					$right_id = get_sub_field('question_id');
				?>
				<li>
					<?php if( $question_right ): ?>
						<a href="#<?php echo $right_id; ?>" class="fancybox" rel="faq">
<?php echo $question_right; ?>
</a>
					<?php endif; ?>
					<div style="display: none">
						<div id="<?php echo $right_id; ?>">
							<h1>
<?php echo $question_right; ?>
</h1>
							<p>
<?php echo $answer_right; ?>
</p>
						</div>
					</div>
				</li>
				<?php endwhile; ?> 
			</ul>
		<?php endif; ?>
	</div>
</div>

<div class="bottom-row-wrap clearfix">
	<?php echo do_shortcode( '[faq_left]' ); ?>
<?php echo do_shortcode( '[faq_middle]' ); ?>
<?php echo do_shortcode( '[faq_right]' ); ?>
</div>
	<?php } else {
		return false;
	}
}

/* Name: contact_page */

add_action( 'genesis_loop', 'dynamik_contact_page_hook_box', 10 );
function dynamik_contact_page_hook_box() {
	dynamik_contact_page_hook_box_content();
}

function dynamik_contact_page_hook_box_content() {
	if ( is_page_template('my-templates/contact.php') ) { ?>
<div class="top-row-wrap clearfix">
		<?php the_field('inquery_content'); ?>
</div>

<div class="bottom-row-wrap clearfix">
	<?php echo do_shortcode( '[eye_wear_left]' ); ?>
<?php echo do_shortcode( '[eye_wear_middle]' ); ?>
<?php echo do_shortcode( '[eye_wear_right]' ); ?>
</div>
	<?php } else {
		return false;
	}
}

/* Name: blog_title */

add_action( 'genesis_before_content', 'dynamik_blog_title_hook_box', 10 );
function dynamik_blog_title_hook_box() {
	dynamik_blog_title_hook_box_content();
}

function dynamik_blog_title_hook_box_content() {
	if ( is_home() || is_archive() ) { ?>
		<div class="blog-wrap">
			<div class="top-row-wrap clearfix">
				<?php echo do_shortcode( '[blog_page_title]' ); ?>
			</div>
		</div>
	<?php } else {
		return false;
	}
}

/* Name: blog_widgets */

add_action( 'genesis_after_content', 'dynamik_blog_widgets_hook_box', 10 );
function dynamik_blog_widgets_hook_box() {
	dynamik_blog_widgets_hook_box_content();
}

function dynamik_blog_widgets_hook_box_content() {
	if ( is_home() || is_archive() ) { ?>
		<div class="blog-wrap">
			<div class="bottom-row-wrap clearfix">
				<?php echo do_shortcode( '[faq_left]' ); ?>
<?php echo do_shortcode( '[faq_middle]' ); ?>
<?php echo do_shortcode( '[faq_right]' ); ?>	
			</div>
		</div>
	<?php } else {
		return false;
	}
}

add_action('genesis_after_content_sidebar_wrap', 'rt_single_widgets_hook_box', 10);
function rt_single_widgets_hook_box() {
	if ( is_single() ) { ?>
			<div class="bottom-row-wrap clearfix">
				<?php echo do_shortcode( '[faq_left]' ); ?>
<?php echo do_shortcode( '[faq_middle]' ); ?>
<?php echo do_shortcode( '[faq_right]' ); ?>	
			</div>
	<?php } else {
		return false;
	}
}

/**
 * Filter in specific body classes based on option values.
 */
add_filter( 'body_class', 'child_body_classes' );
/**
 * Determine which classes will be filtered into the body class.
 *
 * @since 1.0
 * @return array of all classes to be filtered into the body class.
 */
function child_body_classes( $classes ) {
	if ( is_front_page() ) {	
		
	}
	
	
	$classes[] = 'fat-footer-inside';
	$classes[] = 'site-fluid';
	

	if( is_singular() && dynamik_get_custom_field( '_dyn_labels', false, true ) != '' )
	{
		foreach ( dynamik_get_custom_field( '_dyn_labels', false, true ) as $key => $value )
		{
			$classes[] = 'label-' . $key;
		}
	}

	if( defined( 'DYNAMIK_LABEL_WIDTH' ) )
		$classes[] = DYNAMIK_LABEL_WIDTH;

	$classes[] = 'override';
	
	return $classes;
}

add_filter( 'post_class', 'child_post_classes' );
/**
 * Create an array of useful post classes.
 *
 * @since 1.0
 * @return an array of child post classes.
 */
function child_post_classes( $classes )
{
	$classes[] = 'override';

	return $classes;
}



add_action( 'wp_enqueue_scripts', 'child_enqueue_responsive_scripts' );
/**
 * Enqueue Responsive Design javascript code.
 *
 * @since 1.0
 */
function child_enqueue_responsive_scripts() {	
	wp_enqueue_script( 'responsive', CHILD_URL . '/js/responsive.js', array( 'jquery' ), CHILD_THEME_VERSION, true );
}

add_action( 'get_header', 'child_enqueue_custom_scripts' );
/**
 * Enqueue Custom javascript code.
 *
 * @since 1.0
 */
function child_enqueue_custom_scripts() {	
	wp_enqueue_script( 'custom-scripts', CHILD_URL . '/js/custom-scripts.js', array( 'jquery' ), CHILD_THEME_VERSION, true );
}
if ( file_exists( get_stylesheet_directory() . '/lib/functions.php' ) ) {
	require_once( get_stylesheet_directory() . '/lib/functions.php' );
}

	
remove_action('genesis_footer', 'genesis_do_footer');
remove_action('genesis_footer', 'genesis_footer_markup_open', 5);
remove_action('genesis_footer', 'genesis_footer_markup_close', 15);

add_filter( 'excerpt_length', 'rt_excerpt_length' );
function rt_excerpt_length( $length ) {
	return 150;
}

add_filter( 'the_content_more_link', 'rt_read_more_link' );
function rt_read_more_link() {
	return '<a class="more-link" href="' . get_permalink() . '"> more...</a>';
}

remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

add_filter( 'genesis_post_info', 'rt_post_info_filter' );
function rt_post_info_filter($post_info) {
	$post_info = 'by [post_author_posts_link] </br>[post_date] ';
	return $post_info;
}

add_filter( 'comment_form_defaults', 'rt_remove_comment_form_allowed_tags' );
function rt_remove_comment_form_allowed_tags( $defaults ) {
 
	$defaults['comment_notes_after'] = '';
	return $defaults;
 
}

/** Force content-sidebar on single posts only*/
add_filter( 'genesis_pre_get_option_site_layout', 'content_sidebar_layout_single_posts' );

function content_sidebar_layout_single_posts( $opt ) {
if ( is_single() ) {
    $opt = 'content-sidebar'; 
    return $opt;
 
    } 
 
 
}

add_action( 'pre_get_posts', 'exclude_category_posts' );
function exclude_category_posts( $query ) {
 
if( $query->is_main_query() && $query->is_home() ) {
 
$query->set( 'cat', '-4,-5' );
    }
}

// Remove the URL Field in Single Blog Comments
add_filter('comment_form_default_fields', 'url_filtered');
function url_filtered($fields)
{
  if(isset($fields['url']))
   unset($fields['url']);
  return $fields;
}

// Gravity Forms custom functions

require_once( 'lib/gforms.php' );

function arrayToXML(Array $array, SimpleXMLElement &$xml) {

    foreach($array as $key => $value) {

        // None array
        if (!is_array($value)) {
            (is_numeric($key)) ? $xml->addChild("item$key", $value) : $xml->addChild($key, $value);
            continue;
        }   

        // Array
        $xmlChild = (is_numeric($key)) ? $xml->addChild("item$key") : $xml->addChild($key);
        arrayToXML($value, $xmlChild);
    }
}   

add_action('gform_after_submission_3', 'post_to_third_party', 10, 2);
function post_to_third_party($entry, $form) {
	
	ini_set('display_errors',1);
	error_reporting(E_ALL|E_STRICT);

	// $xml = new SimpleXMLElement('<root/>');
	// arrayToXML($entry,$xml);
	// $xmlString = $xml->asXML();

	$xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	$xmlString .= "<root>\n";
	$xmlString .= "<FirstName><![CDATA[".$entry["1"]."]]></FirstName>\n";
	$xmlString .= "<LastName><![CDATA[".$entry["2"]."]]></LastName>\n";
	$xmlString .= "<Phone><![CDATA[".$entry["4"]."]]></Phone>\n";
	$xmlString .= "<Email><![CDATA[".$entry["5"]."]]></Email>\n";
	$xmlString .= "<Address>\n";
	$xmlString .= "	<StreetAddress><![CDATA[".$entry["6.1"]."]]></StreetAddress>\n";
	$xmlString .= "<item6_2><![CDATA[".$entry["6.2"]."]]></item6_2>\n";
	$xmlString .= "<City><![CDATA[".$entry["6.3"]."]]></City>\n";
	$xmlString .= "<State><![CDATA[".$entry["6.4"]."]]></State>\n";
	$xmlString .= "<ZIP><![CDATA[".$entry["6.5"]."]]></ZIP>\n";
	$xmlString .= "<Country><![CDATA[".$entry["6.6"]."]]></Country>\n";
	$xmlString .= "</Address>\n";
	$xmlString .= "<Employer><![CDATA[".$entry["7"]."]]></Employer>\n";
	$xmlString .= "<Occupation><![CDATA[".$entry["8"]."]]></Occupation>\n";
	$xmlString .= "<item9><![CDATA[".$entry["9"]."]]></item9>\n";
	$xmlString .= "<MedicalHistory><![CDATA[".$entry["10"]."]]></MedicalHistory>\n";
	$xmlString .= "<AreYouUnderTheCareOfAPhysician><![CDATA[".$entry["11"]."]]></AreYouUnderTheCareOfAPhysician>\n";
	$xmlString .= "<HowDidYouHearAboutUs><![CDATA[".$entry["12"]."]]></HowDidYouHearAboutUs>\n";
	$xmlString .= "<WhatAreSomeOfYourHobbies><![CDATA[".$entry["13"]."]]></WhatAreSomeOfYourHobbies>\n";
	$xmlString .= "<DoYouHaveVisionInsurance><![CDATA[".$entry["14"]."]]></DoYouHaveVisionInsurance>\n";
	$xmlString .= "<PrimaryVisionInsurance><![CDATA[".$entry["15"]."]]></PrimaryVisionInsurance>\n";
	$xmlString .= "<YourLastEyeExamWasOn><![CDATA[".$entry["16"]."]]></YourLastEyeExamWasOn>\n";
	$xmlString .= "<AreYouTakingAnyMedications><![CDATA[".$entry["18"]."]]></AreYouTakingAnyMedications>\n";
	$xmlString .= "<PleaseListAllYourMedications><![CDATA[".$entry["19"]."]]></PleaseListAllYourMedications>\n";
	$xmlString .= "<AreYouAllergicToAnyMedications><![CDATA[".$entry["20"]."]]></AreYouAllergicToAnyMedications>\n";
	$xmlString .= "<PleaseListTheMedicationsYouAreAllergicTo><![CDATA[".$entry["21"]."]]></PleaseListTheMedicationsYouAreAllergicTo>\n";
	$xmlString .= "<PleaseSelectAnyOfTheFollowingThatApplyToYou>\n";
		$xmlString .= "<IWorkWithSmallPrint><![CDATA[".$entry["22.1"]."]]></IWorkWithSmallPrint>\n";
		$xmlString .= "<IWorkInTheSun><![CDATA[".$entry["22.2"]."]]></IWorkInTheSun>\n";
		$xmlString .= "<IUseProtectiveEyewearAtWork><![CDATA[".$entry["22.3"]."]]></IUseProtectiveEyewearAtWork>\n";
		$xmlString .= "<DiffcultyWithVisionAtArmsLength><![CDATA[".$entry["22.4"]."]]></DiffcultyWithVisionAtArmsLength>\n";
		$xmlString .= "<DifficultyWatchingTelevision><![CDATA[".$entry["22.5"]."]]></DifficultyWatchingTelevision>\n";
		$xmlString .= "<DifficultyWithNearVision><![CDATA[".$entry["22.6"]."]]></DifficultyWithNearVision>\n";
		$xmlString .= "<SensitiveToSunlight><![CDATA[".$entry["22.7"]."]]></SensitiveToSunlight>\n";
		$xmlString .= "<SensitiveToBrightLight><![CDATA[".$entry["22.8"]."]]></SensitiveToBrightLight>\n";
		$xmlString .= "<IAmAStudent><![CDATA[".$entry["22.9"]."]]></IAmAStudent>\n";
		$xmlString .= "<IAmActiveInSports><![CDATA[".$entry["22.11"]."]]></IAmActiveInSports>\n";
		$xmlString .= "<IWantContactLenses><![CDATA[".$entry["22.12"]."]]></IWantContactLenses>\n";
	$xmlString .= "</PleaseSelectAnyOfTheFollowingThatApplyToYou>\n";
	$xmlString .= "<DoYouUseAComputer><![CDATA[".$entry["23"]."]]></DoYouUseAComputer>\n";
	$xmlString .= "<HowManyHoursPerDay><![CDATA[".$entry["24"]."]]></HowManyHoursPerDay>\n";
	$xmlString .= "<EyeInformation><![CDATA[".$entry["25"]."]]></EyeInformation>\n";
	$xmlString .= "<HaveYouEverHadAnyEyeConditionsOrProblems><![CDATA[".$entry["26"]."]]></HaveYouEverHadAnyEyeConditionsOrProblems>\n";
	$xmlString .= "<HaveYouHadAnyEyeOperations><![CDATA[".$entry["27"]."]]></HaveYouHadAnyEyeOperations>\n";
	$xmlString .= "<TellUsAboutYourOperationAndWhenItHappened><![CDATA[".$entry["28"]."]]></TellUsAboutYourOperationAndWhenItHappened>\n";
	$xmlString .= "<HaveYouEverHadAnEyeInjury><![CDATA[".$entry["29"]."]]></HaveYouEverHadAnEyeInjury>\n";
	$xmlString .= "<TellUsALittleBitAboutWhatHappenedAndWhen><![CDATA[".$entry["30"]."]]></TellUsALittleBitAboutWhatHappenedAndWhen>\n";
	$xmlString .= "<DoYouHaveAnyProblemsInAnyOfTheseAreas>\n";
		$xmlString .= "<Blood_Lymph><![CDATA[".$entry["31.1"]."]]></Blood_Lymph>\n";
		$xmlString .= "<Cardiovascular><![CDATA[".$entry["31.2"]."]]></Cardiovascular>\n";
		$xmlString .= "<Ears_Nose_Throat><![CDATA[".$entry["31.3"]."]]></Ears_Nose_Throat>\n";
		$xmlString .= "<Endocrine><![CDATA[".$entry["31.4"]."]]></Endocrine>\n";
		$xmlString .= "<Gastrointestinal><![CDATA[".$entry["31.5"]."]]></Gastrointestinal>\n";
		$xmlString .= "<Headaches><![CDATA[".$entry["31.6"]."]]></Headaches>\n";
		$xmlString .= "<HighBloodPressure><![CDATA[".$entry["31.7"]."]]></HighBloodPressure>\n";
		$xmlString .= "<Immunologic><![CDATA[".$entry["31.8"]."]]></Immunologic>\n";
		$xmlString .= "<Psychiatric><![CDATA[".$entry["31.9"]."]]></Psychiatric>\n";
		$xmlString .= "<Muscle_Bones><![CDATA[".$entry["31.11"]."]]></Muscle_Bones>\n";
		$xmlString .= "<Respiratory><![CDATA[".$entry["31.12"]."]]></Respiratory>\n";
		$xmlString .= "<Skin><![CDATA[".$entry["31.13"]."]]></Skin>\n";
		$xmlString .= "<Urinary><![CDATA[".$entry["31.14"]."]]></Urinary>\n";
		$xmlString .= "<Pregnant_Nursing><![CDATA[".$entry["31.15"]."]]></Pregnant_Nursing>\n";
	$xmlString .= "</DoYouHaveAnyProblemsInAnyOfTheseAreas>\n";
	$xmlString .= "<BecauseYouSelectedOneOrMoreOfTheseProblemsCanYouProvideABitMoreInformationOnYourConditions><![CDATA[".$entry["32"]."]]></BecauseYouSelectedOneOrMoreOfTheseProblemsCanYouProvideABitMoreInformationOnYourConditions>\n";
	$xmlString .= "<FamilyHistory><![CDATA[".$entry["33"]."]]></FamilyHistory>\n";
	$xmlString .= "<HasAnyImmediateFamilyMemberBeenDisgnosedWithAnyOfTheFollowing>\n";
		$xmlString .= "<Glaucoma><![CDATA[".$entry["34.1"]."]]></Glaucoma>\n";
		$xmlString .= "<MacularDegeneration><![CDATA[".$entry["34.2"]."]]></MacularDegeneration>\n";
		$xmlString .= "<RetinalDetachment><![CDATA[".$entry["34.3"]."]]></RetinalDetachment>\n";
		$xmlString .= "<CrossedEye><![CDATA[".$entry["34.4"]."]]></CrossedEye>\n";
		$xmlString .= "<Cataracts><![CDATA[".$entry["34.5"]."]]></Cataracts>\n";
		$xmlString .= "<Diabetes><![CDATA[".$entry["34.6"]."]]></Diabetes>\n";
		$xmlString .= "<Lupus><![CDATA[".$entry["34.7"]."]]></Lupus>\n";
	$xmlString .= "</HasAnyImmediateFamilyMemberBeenDisgnosedWithAnyOfTheFollowing>\n";
	$xmlString .= "<CanYouTellUsWhomInYourFamilyHadGlaucoma><![CDATA[".$entry["35"]."]]></CanYouTellUsWhomInYourFamilyHadGlaucoma>\n";
	$xmlString .= "<CanYouTellUsWhomInYourFamilyHadMacularDegeneration><![CDATA[".$entry["36"]."]]></CanYouTellUsWhomInYourFamilyHadMacularDegeneration>\n";
	$xmlString .= "<CanYouTellUsWhomInYourFamilyHadRetinalDetachment><![CDATA[".$entry["37"]."]]></CanYouTellUsWhomInYourFamilyHadRetinalDetachment>\n";
	$xmlString .= "<CanYouTellUsWhomInYourFamilyWasCrossedEye><![CDATA[".$entry["38"]."]]></CanYouTellUsWhomInYourFamilyWasCrossedEye>\n";
	$xmlString .= "<CanYouTellUsWhomInYourFamilyHasCataracts><![CDATA[".$entry["39"]."]]></CanYouTellUsWhomInYourFamilyHasCataracts>\n";
	$xmlString .= "<PleaseProvideUsWithYourPhysiciansNameAndPhone><![CDATA[".$entry["40"]."]]></PleaseProvideUsWithYourPhysiciansNameAndPhone>\n";
	$xmlString .= "<HIPPA><![CDATA[".$entry["41.1"]."]]></HIPPA>\n";
	$xmlString .= "<IAuthorizeForTheReleaseOfIdentifyingHealthInformation><![CDATA[".$entry["41.2"]."]]></IAuthorizeForTheReleaseOfIdentifyingHealthInformation>\n";
	$xmlString .= "<source><![CDATA[".json_encode($entry)."]]></source>\n";
	$xmlString .= "</root>\n";

	$name =  $entry["1"] . "_" . $entry["2"] . ".xml";

	$filename = getcwd()."/temporary.xml";
	file_put_contents($filename, $xmlString);

	// starting the FTP connection
	$server = "68.224.168.213";
	$port = "21";
	$username = "eye";
	$password = "compulink.5080";
	$remote_file = "./$name";

	$connection = ftp_connect($server,$port) or die("No FTP connection!");
	$loginResult = ftp_login($connection, $username, $password);
	ftp_pasv($connection, true);
	ftp_put($connection, $remote_file, $filename, FTP_ASCII);
	ftp_close($connection);
	unlink($filename);

}


